
const toggleButton = document.getElementsByClassName('toggle-menu-button')[0];
const navbar = document.getElementsByClassName('navbar')[0];

const closeButton = document.getElementsByClassName('close-button')[0];
    
closeButton.addEventListener('click',()=>{
    navbar.classList.remove('active');
})

toggleButton.addEventListener('click',()=>{
    console.log("first")
    navbar.classList.add('active')
})


document.addEventListener('DOMContentLoaded', function() {
    const slider = document.querySelector('.slider');
    const dots = document.querySelectorAll('.dot');
    const imagesToShow = 6;
    const totalImages = dots.length;
    const imageWidthPercentage = 100 / imagesToShow;
  
    dots.forEach(dot => {
      dot.addEventListener('click', function() {
        const index = parseInt(this.getAttribute('data-index'));
        slider.style.transform = `translateX(-${index * imageWidthPercentage}%)`;
        updateActiveDot(index);
      });
    });
  
    function updateActiveDot(index) {
      dots.forEach(dot => dot.classList.remove('active'));
      dots[index].classList.add('active');
    }
  
    // Initialize the first dot as active
    updateActiveDot(0);
  });

  
  